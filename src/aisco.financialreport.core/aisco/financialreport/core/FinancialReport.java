package aisco.financialreport.core;
import aisco.program.activity.Program;

public interface FinancialReport {
    String getDescription();
    int getAmount();
    Program getProgram();
    void printHeader();
}
