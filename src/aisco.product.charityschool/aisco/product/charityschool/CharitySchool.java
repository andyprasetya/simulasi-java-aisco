package aisco.product.charityschool;

import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.expense.*;
import aisco.financialreport.income.*;
import aisco.financialreport.FinancialReportFactory;
import aisco.donation.core.Donation;
import aisco.donation.DonationFactory;
import java.util.ArrayList;
import java.util.List;


public class CharitySchool {

    public static void main(String[] args) {
        System.out.println ("Product Charity School");

        //Create instance of program
        List<Program> programs = new ArrayList<>();
        Program schoolconstruction = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 1, "School Construction", "Construct the building of elementary school", "100 students", "Government", "https://www.myschool.splelive.id/logo");
        Program freelibrary = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl", 2, "Free Library", "Library for underprivileged children ", "children", "BeriBuku Community", "https://www.myschool.splelive.id/liblogo");
        Program paymentelectricity = ProgramFactory.createProgram("aisco.program.operational.ProgramImpl", 3, "Electricity Payment", "Training for new teachers", "10 Teachers");
        //((aisco.program.operational.ProgramImpl)paymentelectricity).setLogoUrl("www.logo");

        //Create instance of Incomes
        List<FinancialReport> incomes = new ArrayList<>();
        FinancialReport income1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","1", "23-10-2019", 100000, "Donation Ana", schoolconstruction, "11000"), "Transfer");
        incomes.add(income1);
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","2", "24-10-2019", 3000000, "Donation Joni", freelibrary, "11000"), "Cash"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","3", "11-11-2019", 5000000, "CSR Firma SJT", schoolconstruction, "110"), "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","3", "11-11-2019", 50000, "Donation Aida", paymentelectricity, "110"), "Cash"));
        income1.printHeader();
        System.out.println(incomes);
        ((aisco.financialreport.income.FinancialReportImpl)income1).sumIncome(incomes);

        //Create instance of Expenses
        List<FinancialReport> expenses = new ArrayList<>();
        FinancialReport expense1 = FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","10", "23-10-2019", 1000000, "Buy Cement", schoolconstruction, "41000"));
        expenses.add(expense1);
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","20", "24-10-2019", 1500000, "Buy Bookcase", freelibrary, "410")));
        expenses.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","30", "12-10-2019", 50000, "Book ABC", freelibrary, "41000")));
        expense1.printHeader();
        System.out.println(expenses);
        ((aisco.financialreport.expense.FinancialReportImpl) expense1).sumExpense(expenses);

        Donation donate = DonationFactory.createDonation("aisco.donation.pgateway.DonationImpl");
        donate.addDonation();
        donate.getDonation();
    }
}
