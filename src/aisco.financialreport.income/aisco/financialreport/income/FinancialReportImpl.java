package aisco.financialreport.income;

import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.core.FinancialReportComponent;
//import aisco.program.activity.Program;
import java.util.ArrayList;
import java.util.List;

public class FinancialReportImpl extends FinancialReportDecorator {
    /* delta add attributes */
    private String paymentMethod;
    
    /*delta adds attributes, modifies the constructor */
    public FinancialReportImpl(FinancialReportComponent record, String paymentMethod) {
        super(record);
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod()
    {
        return paymentMethod;
    }

    /* delta modifies method */
    public void printHeader()
    {   
        System.out.println("\n Income Report");
    }

    public String toString() {
        return "\n" + record +  "Payment Method: " + paymentMethod + ".";
    }

    public void sumIncome(List<FinancialReport> expenses)
    {
        int sum =  totalAmount(expenses);
        System.out.println("Total Income: "+sum+ "\n");
    }
}


