module aisco.financialreport.frequency {
    requires aisco.financialreport.core;
    exports aisco.financialreport.frequency;
    exports aisco.financialreport.frequency.reports;
}
