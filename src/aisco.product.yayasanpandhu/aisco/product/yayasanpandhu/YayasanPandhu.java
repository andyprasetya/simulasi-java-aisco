package aisco.product.yayasanpandhu;

import aisco.program.activity.Program;
import aisco.program.ProgramFactory;
import aisco.financialreport.core.FinancialReport;
import aisco.financialreport.FinancialReportFactory;
import aisco.financialreport.core.*;

import java.util.ArrayList;
import java.util.List;

public class YayasanPandhu {

    public static void main(String[] args) {
        System.out.println("Product Yayasan Pandhu");

        //Create instance of Program
        List<Program> programs = new ArrayList<>();
        Program sharegifts = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl",1, "Share Gifts", "Sharing gifts to slum area", "50 persons", "Pandhu Volunteer", "https://www.yaysanpandhu.splelive.id/pandhu.jpg");
        Program orphanagecompetition = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl",2, "Orphanage Competition", "Competition of Orphanage in Karawang", "30 orphanages", "Government", "https://www.yaysanpandhu.splelive.id/panti.jpg");
        programs.add(sharegifts);
        programs.add(orphanagecompetition);
        System.out.println("\n Programs: ");
        System.out.println(programs);

        //Create instance Incomes with Frequency  
        List<FinancialReport> incomes = new ArrayList<>();
        FinancialReport core1 = FinancialReportFactory.createFinancialReport
        ("aisco.financialreport.core.FinancialReportImpl","1", "12-11-2019", 2500000, 
        "Donation from Society", sharegifts, "11000");
        FinancialReport income1 = FinancialReportFactory.createFinancialReport
        ("aisco.financialreport.income.FinancialReportImpl", core1, "Cash");
        FinancialReport freq1 = FinancialReportFactory.createFinancialReport
        ("aisco.financialreport.frequency.reports.FinancialReportImpl",income1, "Monthly");
        incomes.add(freq1);
        freq1.printHeader(); //original() call, output "Income Report"

        // //Try DFrequency first and then DIncome
        // FinancialReport core2 = FinancialReportFactory.createFinancialReport
        // ("aisco.financialreport.core.FinancialReportImpl","2", "13-11-2019", 300000,
        // "Donation Raden", orphanagecompetition, "11000");
        // FinancialReport freq2 = FinancialReportFactory.createFinancialReport
        // ("aisco.financialreport.frequency.FinancialReportImpl",core2, "Monthly");
        // FinancialReport income2 = FinancialReportFactory.createFinancialReport
        // ("aisco.financialreport.income.FinancialReportImpl", freq2, "Cash");
        // incomes.add(income2);
        // freq2.printHeader(); //original() call, output "FinancialReport"


       

        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl",FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","2", "13-11-2019", 300000, "Donation Raden", orphanagecompetition, "11000"), "Transfer"));
        incomes.add(FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl","3", "14-11-2019", 500000, "Donation Anonim", sharegifts, "110"), "Transfer"));
        freq1.printHeader();
        System.out.println(incomes);
        ((aisco.financialreport.income.FinancialReportImpl)income1).sumIncome(incomes);
    }
}
