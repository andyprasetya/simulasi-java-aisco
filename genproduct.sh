function external_module()
{
    if [ $1 == "aisco.donation.pgateway" ]; then
    cp external/payment.page.jar $product/
    echo "add external module"
    fi
}

function validate_product()
{
 if [ $1 == "aisco.program.activity" ];then
  program=true
 elif [ $1 == "aisco.financialreport.income" ];then
  income=true
 fi
}

function build_product(){
  echo -e "building the product: $mainclass"
  javac -d classes  --module-path $product $(find src/$product -name "*.java") src/$product/module-info.java 
  jar --create --file $product/$mainclass.jar --main-class $product.$mainclass -C classes .
  rm -r classes
  echo "Product $mainclass is ready"
}
function build_product_requirement(){
  product=$1
  targetpath="src/$product/module-info.java"
  req=$(cat $targetpath | grep "requires \( transitive | static \)\?"| awk '{print $2}' | cut -d';' -f 1 )
  for requ in $req; do
  validate_product $requ
  done 
  if [ "$program" == true ] && [ "$income" == true ]; then
    for reqprod in $req; do
    echo -e "building requirement for $mainclass: $reqprod"
    external_module $reqprod
    javac -d classes --module-path $product $(find src/$reqprod -name "*.java") src/$reqprod/module-info.java 
    jar --create --file $product/$reqprod.jar -C classes .
    rm -r classes
    done
    echo "requirement building done"
    build_product
  else
  echo "product is not valid"
  echo "build failed"
  fi
}

product=$1
mainclass=$2
program=false
income=false
if [ -d "$1" ]; then rm -r $1; fi
if [ -d "classes" ]; then rm -r classes; fi 
mkdir $1
echo " -- checking requirement -- "
build_product_requirement $product
echo "Build time: $SECONDS seconds"
